Categories:Internet,Money
License:Apache-2.0
Web Site:https://thealaskalinuxuser.wordpress.com
Source Code:https://github.com/alaskalinuxuser/app_justcraigslist
Issue Tracker:https://github.com/alaskalinuxuser/app_justcraigslist/issues

Auto Name:Just Craigslist
Summary:Search posts on Craigslist
Description:
Search [https://www.craigslist.org Craigslist] and save on bandwidth at the same
time. The search is only text, which presents you with a list of your search
criteria. Then, when you select a posting that you wish to see, it will be
displayed as normal in a special web view window. With simple colors and
controls, this helps you focus on what is really important: the posts!
.

Repo Type:git
Repo:https://github.com/alaskalinuxuser/app_justcraigslist

Build:1.1,1
    commit=62664edf440cf0246854a10a1482da50665b57f4
    subdir=app
    gradle=yes

Build:1.5,5
    commit=0904b9328c2198f54974c11191a6196b12480fed
    subdir=app
    gradle=yes

Build:1.6,6
    commit=27badb5f7c9dd5a3a217e4f069e45930cc04f09c
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.6
Current Version Code:6
